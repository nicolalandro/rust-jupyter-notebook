# Rust Jupyter
This container is [Rust](https://www.rust-lang.org/it) [Jupyter Notebook](https://jupyter.org/) service, 
that include also other kernels like (python obviusly) [sos](https://vatlab.github.io/sos-docs/) and [coq](https://coq.inria.fr/).

* from public
```
docker run -p 8888:8888 registry.gitlab.com/nicolalandro/rust-jupyter-notebook/evcxr:full
```

* from codebase
```
docker-compose up
```

# References
[evcxr jupyter plot tutorial](https://plotters-rs.github.io/plotters-doc-data/evcxr-jupyter-integration.html)
