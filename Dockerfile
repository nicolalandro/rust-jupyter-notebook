FROM rust

# Install Jupyter notebook 
RUN apt-get update && apt-get install -y libzmq3-dev jupyter-notebook
RUN cargo install evcxr_jupyter
RUN evcxr_jupyter --install

# Configure PIP
RUN apt-get update && apt-get install -y python3-pip

# Configure jupyter plugin for install extension
RUN pip install --upgrade pip && pip install jupyter_contrib_nbextensions
RUN jupyter contrib nbextension install --system
RUN jupyter nbextensions_configurator enable --system

# Configure SOS
RUN pip install --upgrade pip && pip install sos-notebook
RUN python3 -m sos_notebook.install

# Configure coq (proof assistant)
RUN apt-get update && apt-get install -y coq coqide
RUN pip install --upgrade pip && pip install coq-jupyter
RUN python3 -m coq_jupyter.install

WORKDIR /notebooks
CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''